package com.farmcart.ee.enums;

public enum CampaignRuleEnum {

	TotalWeeklyRevenueGreaterThan("Total Weekly Revenue Greater Than", 1L),
	TotalMontlyRevenueGreaterThan("Total Montly Revenue Greater Than",2L),
	All("All",3L),
	AverageWeeklyRevenue("Average Weekly Revenue",4L),
	TotalWeeklyRevenueLessThan("Total Weekly Revenue Less Than",5L),
	TotalWeeklyRevenueEqual("Total Weekly Revenue Equal",6L),
	TotalMontlyRevenueLessThan("Total Montly Revenue Less Than",7L),
	TotalMontlyRevenueEqual("Total Montly Revenue Equal",8L), 
	SendToSpecificCustomers("Send to Specific Customers",9L),
	SpecificProductOfferProductName("Specific Product Offer Product Name",10L);
	private String name;

	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	CampaignRuleEnum(String name, Long id) {
		this.name = name;
		this.id = id;
	}

}
