package com.farmcart.ee.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.farmcart.ee.bean.CustomerBean;
import com.farmcart.ee.company.companycrud.CampaignDetail;
import com.farmcart.ee.company.companycrud.CampaignRule;

@Component
public class CampaignEvalutionService {

	@Autowired
	CampaignRuleEngine campaignRuleEngine;

	@Autowired
	MailService emailService;

	public void evaluteCampaign(CampaignDetail campDetails, Optional<CampaignRule> camRule) {

		List<CustomerBean> customers = getUserForRule(campDetails, camRule);
		processRequest(customers, campDetails.getName(), campDetails.getMessage());

	}

	private void processRequest(List<CustomerBean> customers, String subject, String message) {
		List<String> emails = new ArrayList<String>();
		for (CustomerBean ub : customers) {
			emails.add(ub.getEmailId());
		}
		 subject="Farmcart Campaign";
		 message="10% Off on every item";
		 emails.clear();
		 emails.add("dadakambale18@gmail.com");
		emailService.sendMail(emails, subject, message);
	}

	private List<CustomerBean> getUserForRule(CampaignDetail campDetails, Optional<CampaignRule> camRule) {

		return campaignRuleEngine.getUserForCampaignRule(camRule.get(), campDetails.getValue());
	}

}
