package com.farmcart.ee.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.farmcart.ee.bean.CustomerBean;
import com.farmcart.ee.company.companycrud.CampaignRule;
import com.farmcart.ee.rules.TotalWeeklyRevenueGreaterThan;

@Component
public class CampaignRuleEngine {

	List<CampaigRuleI> ruleRegistery = new ArrayList<CampaigRuleI>();
	@Autowired
	TotalWeeklyRevenueGreaterThan totalWeeklyRevenueGreaterThan;

	public CampaignRuleEngine() {

	}

	public List<CustomerBean> getUserForCampaignRule(CampaignRule rule, String value) {
		regsiterRule();
		for (CampaigRuleI campRule : ruleRegistery) {
			if (campRule.check(rule.getId())) {
				return campRule.process(value);
			}
		}

		return null;
	}

	public void regsiterRule() {
		ruleRegistery.add(totalWeeklyRevenueGreaterThan);
	}

}
