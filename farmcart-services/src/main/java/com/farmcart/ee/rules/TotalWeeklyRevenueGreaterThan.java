package com.farmcart.ee.rules;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.farmcart.ee.bean.CustomerBean;
import com.farmcart.ee.bean.constant.Constants;
import com.farmcart.ee.enums.CampaignRuleEnum;
import com.farmcart.ee.service.CampaigRuleI;

@Component
public class TotalWeeklyRevenueGreaterThan implements CampaigRuleI {

	@Autowired
	CampaignRuleRepo campaignRuleRepo;

	@Override
	public Boolean check(Long ruleId) {
		if (CampaignRuleEnum.TotalWeeklyRevenueGreaterThan.getId() == ruleId)
			return true;
		return false;
	}

	@Override
	public List<CustomerBean> process(String value) {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DATE, -8); // Adding 5 days
		String output = Constants.sdf.format(c.getTime());
		String sql = "select o.customerid,c.emailId ,sum(o.g_total) as tt from customer c,order_bean o where c.id=o.customerid and o.created_at>'"+output + "' group by o.customerid ,c.emailId  having sum(o.g_total)>"+value;
		System.out.println("sql==>"+sql);
		return campaignRuleRepo.process(sql);
	}

}
