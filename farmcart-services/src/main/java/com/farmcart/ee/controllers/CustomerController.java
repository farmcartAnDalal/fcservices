package com.farmcart.ee.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.farmcart.ee.bean.CustomerBean;
import com.farmcart.ee.bean.constant.Constants;
import com.farmcart.ee.bean.constant.Constants.ACTION;
import com.farmcart.ee.bean.constant.Constants.ENTITY;
import com.farmcart.ee.bean.constant.EEUtil;
import com.farmcart.ee.company.companycrud.CustomerCRUD;
import com.farmcart.ee.interfaces.KeyValueAggregation;
import com.farmcart.ee.service.AuditLogService;

@RestController
public class CustomerController {

	@Autowired
	CustomerCRUD customerCRUD;
	@Autowired
	private AuditLogService auditLogService;

	@PostMapping(value = "/customer")
	public CustomerBean saveCustomerDetails(@RequestBody CustomerBean customerBean,
			@RequestHeader("Authorization") String token) {
		String userName = Constants.getUserName(token);
		Date date = new Date();
		customerBean.setCreatedAt(date);
		customerBean.setUpdatedAt(date);
		customerBean.setUserName(userName);
		customerBean.setIsActive(1);
		customerCRUD.save(customerBean);

		auditLogService.saveAuditLog(EEUtil.getAuditLogBean(userName, ACTION.CREATE.name(),customerBean.getName(), ENTITY.CUSTOMER.name()));
		return customerBean;
	}

	@GetMapping(value = "/customer/{id}")
	public CustomerBean getCustomerBean(@PathVariable Integer id, @RequestHeader("Authorization") String token) {
		String userName = Constants.getUserName(token);
		return customerCRUD.getById(id, userName);
	}

	@GetMapping(value = "/customers")
	public List<CustomerBean> getCustomers(@RequestHeader("Authorization") String token) {
		String userName = Constants.getUserName(token);
		return customerCRUD.getCustomerList(userName);
	}

	@DeleteMapping(value = "/deletecustomer/{id}")
	public Boolean deleteCustomer(@PathVariable Integer id, @RequestHeader("Authorization") String token) {
		String userName = Constants.getUserName(token);
		CustomerBean customerBean = customerCRUD.findById(id).get();
		customerBean.setIsActive(0);
		customerCRUD.save(customerBean);
		auditLogService.saveAuditLog(EEUtil.getAuditLogBean(userName, ACTION.DELETE.name(),customerBean.getName(), ENTITY.CUSTOMER.name()));
		return true;
	}

	@GetMapping(value = "/customercountpermonth")
	public List<KeyValueAggregation> getCustomerCountPerMonth(@RequestHeader("Authorization") String token) {
		String userName = Constants.getUserName(token);
		return customerCRUD.getCustomerCountPerMonth(userName);
	}

}
