package com.farmcart.ee.rules;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import com.farmcart.ee.bean.CustomerBean;

@Repository
public class CampaignRuleRepo {

	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public List<CustomerBean> process(String sqlQuery) {
		try {
			return jdbcTemplate.query(sqlQuery, new ResultSetExtractor<List<CustomerBean>>() {
				@Override
				public List<CustomerBean> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<CustomerBean> list = new ArrayList<CustomerBean>();
					while (rs.next()) {
						CustomerBean e = new CustomerBean();
						e.setId(rs.getInt(1));
						e.setEmailId(rs.getString(2));
						list.add(e);
					}
					return list;
				}
			});
			
		} catch (Exception e) {
			System.out.println("Error occured while doing query==>"+sqlQuery);
			return new ArrayList<CustomerBean>();
			
		}
		
	}
}
