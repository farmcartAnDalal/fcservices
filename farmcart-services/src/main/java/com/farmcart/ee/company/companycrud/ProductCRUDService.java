package com.farmcart.ee.company.companycrud;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.farmcart.ee.bean.ProductBean;
import com.farmcart.ee.interfaces.KeyValueAggregation;

public interface ProductCRUDService extends CrudRepository<ProductBean, Integer> {

	@Query("select p.name as key,p.quantity as value from ProductBean p where p.userName= :userName and  p.quantity <= :count")
	public List<KeyValueAggregation> getProductOnAlertTopSeven(@Param("userName")String userName ,@Param("count")Integer count);
	
	@Query("select p from ProductBean p where p.userName= :userName")
	public List<ProductBean> getProductList(@Param("userName")String userName);

	@Query("select p from ProductBean p where p.userName= :userName and p.id= :id")
	public ProductBean getProductByIdAndUserName(Integer id,String userName);
}
