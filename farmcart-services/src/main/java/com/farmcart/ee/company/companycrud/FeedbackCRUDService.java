package com.farmcart.ee.company.companycrud;

import org.springframework.data.repository.CrudRepository;

import com.farmcart.ee.bean.FeedbackBean;

public interface FeedbackCRUDService extends CrudRepository<FeedbackBean, Integer> {
    

}
