package com.farmcart.ee.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.farmcart.ee.bean.AuditLogBean;
import com.farmcart.ee.company.companycrud.AuditlogCRUD;

@Component
public class AuditLogService {
	
	@Autowired
	AuditlogCRUD auditlogCRUD;
	
	public void saveAuditLog(AuditLogBean auditLog) {
		auditlogCRUD.save(auditLog);
	}
}

