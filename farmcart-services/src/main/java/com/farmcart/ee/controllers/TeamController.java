package com.farmcart.ee.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.farmcart.ee.bean.TeamMember;
import com.farmcart.ee.service.TeamService;

@RestController
public class TeamController {
	
	@Autowired
	TeamService teamService;
	
	@GetMapping(value="/teammembers")
	public ResponseEntity<List<TeamMember>> getTeamMembers()
	{
		List<TeamMember> teams=teamService.getListOfTeamMembers();
		return new ResponseEntity<List<TeamMember>>(teams,HttpStatus.OK);
	}
	
	@PostMapping(value="teammember")
	public ResponseEntity<Boolean> save(@RequestBody TeamMember member)
	{
		teamService.save(member);
		return new ResponseEntity<Boolean>(true,HttpStatus.OK);
	}
	
	@DeleteMapping(value="member/{id}")
	public ResponseEntity<Boolean> deleteTeam(@PathVariable("id") Long id )
	{
		teamService.delete(id);
		return new ResponseEntity<Boolean>(true,HttpStatus.OK);
	}

}
