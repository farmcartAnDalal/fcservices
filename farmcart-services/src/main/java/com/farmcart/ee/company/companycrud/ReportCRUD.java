package com.farmcart.ee.company.companycrud;

import org.springframework.data.repository.CrudRepository;

import com.farmcart.ee.report.Report;

public interface ReportCRUD extends CrudRepository<Report, Integer> {

}
