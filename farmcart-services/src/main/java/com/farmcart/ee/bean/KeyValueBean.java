package com.farmcart.ee.bean;

public class KeyValueBean<T, E> {

	T key;
	E value;

	public T getKey() {
		return key;
	}

	public void setKey(T key) {
		this.key = key;
	}

	public E getValue() {
		return value;
	}

	public void setValue(E value) {
		this.value = value;
	}

}
