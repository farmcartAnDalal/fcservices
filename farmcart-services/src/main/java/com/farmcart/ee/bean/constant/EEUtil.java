package com.farmcart.ee.bean.constant;

import java.util.Date;

import com.farmcart.ee.bean.AuditLogBean;

public class EEUtil {

	public static AuditLogBean getAuditLogBean(String userName, String action, String entityName,String entity) {
		AuditLogBean audit = new AuditLogBean();
		audit.setUserName(userName);
		audit.setEntityName(entityName);
		audit.setEntity(entity);
		audit.setAction(action);
		audit.setCreatedAt(new Date());
		audit.setCreatedBy(userName);
		return audit;
	}

}
