package com.farmcart.ee.controllers;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.farmcart.ee.bean.ProductBean;
import com.farmcart.ee.bean.constant.Constants;
import com.farmcart.ee.bean.constant.Constants.ACTION;
import com.farmcart.ee.bean.constant.Constants.ENTITY;
import com.farmcart.ee.bean.constant.EEUtil;
import com.farmcart.ee.company.companycrud.OrderItemCRUD;
import com.farmcart.ee.company.companycrud.ProductCRUDService;
import com.farmcart.ee.interfaces.KeyValueAggregation;
import com.farmcart.ee.service.AuditLogService;

@RestController
public class ProductServiceController {

	@Autowired
	private ProductCRUDService pService;

	@Autowired
	OrderItemCRUD orderItemCRUD;
	
	@Autowired
	private AuditLogService auditLogService;
	

	@RequestMapping(method = RequestMethod.GET, value = "/product")
	public List<ProductBean> getAllProducts(@RequestHeader("Authorization") String token) {
		String userName = Constants.getUserName(token);
		return pService.getProductList(userName);

	}

	@RequestMapping(method = RequestMethod.GET, value = "/product/{id}")
	public Optional<ProductBean> getAllProduct(@PathVariable Integer id) {
		return pService.findById(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/product")
	public void registerProduct(@RequestBody ProductBean product,@RequestHeader("Authorization") String token) {
		String userName = Constants.getUserName(token);		
		product.setUserName(userName);
		pService.save(product);
		auditLogService.saveAuditLog(EEUtil.getAuditLogBean(userName, ACTION.CREATE.name(),product.getName(), ENTITY.PRODUCT.name()));
	}

	@RequestMapping(value = "/product/{id}", method = RequestMethod.PUT)
	public void updateProduct(@RequestBody ProductBean product, @PathVariable Integer id) {
		product.setId(id);
		pService.save(product);
	}

	@RequestMapping(value = "/deleteproduct/{id}", method = RequestMethod.DELETE)
	public void deleteProduct(@PathVariable Integer id ,@RequestHeader("Authorization") String token) {
		String userName = Constants.getUserName(token);	
		ProductBean product=pService.getProductByIdAndUserName(id, userName);
		if(product!=null) {
			auditLogService.saveAuditLog(EEUtil.getAuditLogBean(userName, ACTION.DELETE.name(), product.getName(),ENTITY.PRODUCT.name()));
			pService.deleteById(id);
		}
	}

	@RequestMapping(value = "/productonstockalert/{day}", method = RequestMethod.GET)
	public List<KeyValueAggregation> getProductOnStockAlert(@RequestHeader("Authorization") String token ,@PathVariable("day") Integer count) {
		String userName = Constants.getUserName(token);
		return pService.getProductOnAlertTopSeven(userName,count);
	}

	@RequestMapping(value = "/productsoldperday/{day}", method = RequestMethod.GET)
	public List<KeyValueAggregation> getProductSoldPerDayt(@RequestHeader("Authorization") String token,@PathVariable("day") Integer day) {
		String userName = Constants.getUserName(token);
		Calendar cal = Calendar.getInstance();
		if(day==1) {
			    cal.add(Calendar.DAY_OF_MONTH, -1);
			    Date tomorrow=cal.getTime();
			return orderItemCRUD.getProductSellperDay(userName,tomorrow);
		}else if(day==2) {
			 cal.add(Calendar.DAY_OF_MONTH, -8);
			    Date sevenDayBack=cal.getTime();
			return orderItemCRUD.getProductSellperDay(userName,sevenDayBack);
		}else if(day==3) {
			 cal.add(Calendar.DAY_OF_MONTH, -30);
			    Date oneMonthBack=cal.getTime();
			return orderItemCRUD.getProductSellperDay(userName,oneMonthBack);
		}else if(day==4) {
			 cal.add(Calendar.DAY_OF_MONTH, -365);
			    Date yearback=cal.getTime();
			return orderItemCRUD.getProductSellperDay(userName,yearback);
		}
		return null;
		
	}
}
