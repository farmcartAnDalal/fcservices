package com.farmcart.ee.service;

import java.util.List;

import com.farmcart.ee.bean.CustomerBean;

public interface CampaigRuleI {

	public Boolean check(Long ruleId);

	public List<CustomerBean> process(String value);

}
