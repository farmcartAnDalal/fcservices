package com.farmcart.ee.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.farmcart.ee.bean.KeyValueBean;
import com.farmcart.ee.bean.constant.Constants;

@Component
public class GraphService {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public List<KeyValueBean<Date, Integer>> getProudctCountsForDays(int days)
	{
		String query="select ob.created_at ,sum (oi.quantity) from order_bean_order_items oboi ,order_item oi ,order_bean ob " + 
				"where ob.orderid=oboi.order_orderid and oboi.order_items_order_itemid=oi.order_itemid " + 
				"and ob.created_at >'"+ getDayByDays(days)+"'"+
				" group by ob.created_at " + 
				" order by ob.created_at"; 
		return getListOfKeyValue(days,query);
		
	}
	
	public List<KeyValueBean<Date, Integer>> getOrderPerDay(int days) {
		
		String output = getDayByDays(days);
		String query="select ob.created_at, count(ob.orderid) from order_bean ob " + 
				"where ob.created_at >'"+output+"' " + 
				"group by ob.created_at " + 
				"order by ob.created_at"; 
		return getListOfKeyValue(days,query);
		
	}
	
	public List<KeyValueBean<Date, Integer>> getCustomersPerDay(int days) {
		
		String output = getDayByDays(days);
		String query="select c.created_at, count(c.id) from customer c " + 
				"where c.created_at >'"+output+ "' " + 
				"group by c.created_at " + 
				"order by c.created_at";
		return getListOfKeyValue(days,query);
		
	}
	
	public List<Double> getPerDayCashFlow()
	{
		String query="select sum(ob.g_total) from order_bean ob\n" + 
				"group by ob.created_at\n" + 
				"order by ob.created_at";
		return jdbcTemplate.query(query, new ResultSetExtractor<List<Double>>() {
			@Override
			public List<Double> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Double> result=new ArrayList<>();
				while (rs.next()) {
					result.add(rs.getDouble(1));
				}
				return result;
			}
		});
	}
	
	private List<KeyValueBean<Date, Integer>> getListOfKeyValue(int days ,String query) {
		try {
			getDayByDays(days);
			
					
			return jdbcTemplate.query(query, new ResultSetExtractor<List<KeyValueBean<Date, Integer>>>() {
				@Override
				public List<KeyValueBean<Date, Integer>> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<KeyValueBean<Date, Integer>> result=new ArrayList<>();
					while (rs.next()) {
						KeyValueBean<Date, Integer> kv=new KeyValueBean<>();
						kv.setKey(rs.getDate(1));
						kv.setValue(rs.getInt(2));
						result.add(kv);
					}
					return result;
				}
			});
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error occured while doing query==>TodaysOrder");
			return null;
			
		}
		
	}
	private String getDayByDays(int days) {
		long now=new Date().getTime();
		long after=(now-(3600000*24*days));
		String output = Constants.sdf.format(new Date(after));
		System.out.println("========>"+output);
		return output;
	}
	
}
