package com.farmcart.ee.proxy;

import java.util.Date;

public interface Product {

	public Integer getId();

	public void setId(Integer id);

	public String getName();

	public void setName(String name);

	public Integer getQuantity();

	public void setQuantity(Integer quantity);

	public Float getBuyPrice();

	public void setBuyPrice(Float buyPrice);

	public Float getSellPrice();

	public void setSellPrice(Float sellPrice);

	public Integer getUniqueNumber();

	public void setUniqueNumber(Integer uniqueNumber);

	public Date getExpireDate();

	public void setExpireDate(Date expireDate);

}
