package com.farmcart.ee.company.companycrud;

import org.springframework.data.repository.CrudRepository;

import com.farmcart.ee.bean.CompanyBean;

public interface CompanyCRUDServices extends CrudRepository<CompanyBean, Integer> {

}

