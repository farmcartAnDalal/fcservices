package com.farmcart.ee.interfaces;

public interface KeyValueAggregation {
	
	String getKey();

	Long getValue();

}
