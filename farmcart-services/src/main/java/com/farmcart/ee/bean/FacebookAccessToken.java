package com.farmcart.ee.bean;

import com.fasterxml.jackson.annotation.JsonAlias;

public class FacebookAccessToken {

	@JsonAlias(value = { "access_token" })
	String access_token;

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
}
