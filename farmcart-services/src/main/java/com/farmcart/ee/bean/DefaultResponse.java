package com.farmcart.ee.bean;

import java.util.List;

public class DefaultResponse<T> {
	
	String error;
	
	List<T> data;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

}
