package com.farmcart.ee.exception;

public class FarmcartQueryException  extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	FarmcartQueryException(String errorMsg)
	{
		super(errorMsg);
	}

}
