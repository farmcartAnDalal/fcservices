package com.farmcart.ee.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.farmcart.ee.bean.AuditLogBean;
import com.farmcart.ee.bean.constant.Constants;
import com.farmcart.ee.company.companycrud.AuditlogCRUD;

@RestController
public class AuditLogController {

	@Autowired
	AuditlogCRUD auditlogCRUD;

	@GetMapping(value = "/getaudits")
	public List<AuditLogBean> getAuditLog(@RequestHeader("Authorization") String token) {
		String userName = Constants.getUserName(token);
		return auditlogCRUD.getAuditLogs(userName);
	}

}
