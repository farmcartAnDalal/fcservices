package com.farmcart.ee.company.companycrud;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.farmcart.ee.bean.AuditLogBean;

public interface AuditlogCRUD  extends CrudRepository<AuditLogBean, Integer>{

	
	@Query("select a from AuditLogBean a where a.userName =:userName")
	public List<AuditLogBean> getAuditLogs(@Param("userName")String userName);
}
