package com.farmcart.ee.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.farmcart.ee.bean.KeyValueBean;
import com.farmcart.ee.service.GraphService;

@RestController
public class GraphController {
	
	@Autowired
	GraphService graphService;
	
	@GetMapping(value="/product/{days}/count")
	public ResponseEntity<List<KeyValueBean<Date, Integer>>> getProductSoldPerDay(@PathVariable Integer days)
	{
		List<KeyValueBean<Date, Integer>> result=graphService.getProudctCountsForDays(days);
		return new ResponseEntity<List<KeyValueBean<Date,Integer>>>(result,HttpStatus.OK);
	}
	
	@GetMapping(value="/customer/{days}/count")
	public ResponseEntity<List<KeyValueBean<Date, Integer>>> getCustomersPerDay(@PathVariable Integer days)
	{
		List<KeyValueBean<Date, Integer>> result=graphService.getCustomersPerDay(days);
		return new ResponseEntity<List<KeyValueBean<Date,Integer>>>(result,HttpStatus.OK);
	}
	
	@GetMapping(value="/order/{days}/count")
	public ResponseEntity<List<KeyValueBean<Date, Integer>>> getOrdersPerDay(@PathVariable Integer days)
	{
		List<KeyValueBean<Date, Integer>> result=graphService.getOrderPerDay(days);
		return new ResponseEntity<List<KeyValueBean<Date,Integer>>>(result,HttpStatus.OK);
	}
	
	@GetMapping(value="/perdaycashflow")
	public ResponseEntity<List<Double>> getEverydayCashFlow()
	{
		List<Double> result=graphService.getPerDayCashFlow();
		return new ResponseEntity<List<Double>>(result,HttpStatus.OK);
	}

}
