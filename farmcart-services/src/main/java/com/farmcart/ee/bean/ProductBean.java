package com.farmcart.ee.bean;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;

import com.farmcart.ee.proxy.Product;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class ProductBean extends BaseEntity implements Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@PrimaryKeyJoinColumn
	private Integer id;
	private String name;
	private String description;
	private Integer quantity;
	private Float buyPrice;
	private Float sellPrice;
	@JsonProperty("govtGST")
	private Double govtGST;
	@JsonProperty("stateGST")
	private Double stateGST;
	private Double discount;
	private Integer isActive;
	private Integer uniqueNumber;
	private Boolean stockAlert;
	@JsonFormat(pattern = "dd-mm-yyyy")
	private Date expireDate;
	private Long vendorId;
	private Integer unitId;

	public Integer getUnitId() {
		return unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Float getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(Float buyPrice) {
		this.buyPrice = buyPrice;
	}

	public Float getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(Float sellPrice) {
		this.sellPrice = sellPrice;
	}

	public Double getGovtGST() {
		return govtGST;
	}

	public void setGovtGST(Double govtGST) {
		this.govtGST = govtGST;
	}

	public Double getStateGST() {
		return stateGST;
	}

	public void setStateGST(Double stateGST) {
		this.stateGST = stateGST;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Integer getIsActive() {
		return isActive;
	}

	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}

	public Integer getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(Integer uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getStockAlert() {
		return stockAlert;
	}

	public void setStockAlert(Boolean stockAlert) {
		this.stockAlert = stockAlert;
	}

}
