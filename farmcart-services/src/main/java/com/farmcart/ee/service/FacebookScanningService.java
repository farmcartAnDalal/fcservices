package com.farmcart.ee.service;

import org.springframework.social.facebook.api.Comment;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.PagingParameters;
import org.springframework.social.facebook.api.Post;
import org.springframework.social.facebook.api.Reference;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.farmcart.ee.bean.FacebookAccessToken;
import com.farmcart.ee.bean.constant.Constants;

@Component
public class FacebookScanningService {

	FacebookTemplate facebookTemplate = null;

	public PagedList<Comment> getCommentsForPost(String postId) {
		System.out.println(postId);
		facebookTemplate = new FacebookTemplate(Constants.map.get("access_token"));
		PagingParameters params = new PagingParameters(5, null, null, null);
		PagedList<Comment> commentsMy = facebookTemplate.commentOperations().getComments(postId, params);
		return commentsMy;
	}

	public PagedList<Post> getFaceBookPosts() {
		facebookTemplate = new FacebookTemplate(Constants.map.get("access_token"));
		PagingParameters params = new PagingParameters(25, null, null, null);
		return facebookTemplate.feedOperations().getFeed(params);
	}

	public PagedList<Reference> getLikesForPost(String postId) {
		facebookTemplate = new FacebookTemplate(Constants.map.get("access_token"));

		PagedList<Reference> likes = facebookTemplate.likeOperations().getLikes(postId);
		return likes;
	}

	public void startScanningOfFaceBook() {
		facebookTemplate = new FacebookTemplate(Constants.map.get("access_token"));
		PagedList<Post> posts = facebookTemplate.feedOperations().getFeed();
	}

	public String getAccessTokenFromCode(String code) {
		RestTemplate rs = new RestTemplate();
		String accessTokenUrl = "https://graph.facebook.com/v1.0/oauth/access_token?client_id=2069296173370724&redirect_uri=http://localhost:8080/connectFacebookCallBack&state=1234&client_secret=1c0fc0f9ce73f55c2909619cb1a2c6e2&state=6646c20d-f61e-4984-ae64-abcc92f0d8a3&code="
				+ code;
		System.out.println(":==>" + accessTokenUrl);
		try {
			FacebookAccessToken facebookAccessToken = rs.getForObject(accessTokenUrl, FacebookAccessToken.class);
			return facebookAccessToken.getAccess_token();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	private static final String[] ALL_POST_FIELDS = { "id", "actions", "admin_creator", "application", "caption",
			"created_time", "description", "from", "icon", "is_hidden", "is_published", "link", "message",
			"message_tags", "name", "object_id", "picture", "place", "privacy", "properties", "source", "status_type",
			"story", "to", "type", "updated_time", "with_tags", "shares" };

}
