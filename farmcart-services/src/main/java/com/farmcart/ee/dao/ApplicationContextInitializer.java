package com.farmcart.ee.dao;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ApplicationContextInitializer {
	private final static ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

	private ApplicationContextInitializer() {

	}

	public static ApplicationContext getApplicationContext() {
		return ctx;
	}
}
