package com.farmcart.ee.rules;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.farmcart.ee.bean.CustomerBean;
import com.farmcart.ee.bean.constant.Constants;
import com.farmcart.ee.enums.CampaignRuleEnum;
import com.farmcart.ee.service.CampaigRuleI;

public class AverageWeeklyRevenue  implements CampaigRuleI {

	@Autowired
	CampaignRuleRepo campaignRuleRepo;

	@Override
	public Boolean check(Long ruleId) {
		if (CampaignRuleEnum.AverageWeeklyRevenue.getId() == ruleId)
			return true;
		return false;
	}

	@Override
	public List<CustomerBean> process(String value) {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DATE, -8); 
		String output = Constants.sdf.format(c.getTime());
		String sql = "select o.customerid,c.emailId ,sum(o.g_total) as tt from customer c,order_bean o where c.id=o.customerid and o.created_at>'"+output + "' group by o.customerid ,c.emailId  having (sum(o.g_total)/8)>"+value;
		System.out.println("sql==>"+sql);
		return campaignRuleRepo.process(sql);
	}

}
