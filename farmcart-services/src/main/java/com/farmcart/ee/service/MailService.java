package com.farmcart.ee.service;

import java.util.List;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Repository;

@Repository
public class MailService {

	//todo https://myaccount.google.com/lesssecureapps
	
	
	@Autowired
	private JavaMailSender sender;

	private void sendEmail() throws Exception {

		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);
		helper.setTo("dadakambale18@gmail.com");
		helper.setText("How are you?");
		helper.setSubject("Hi");
		sender.send(message);
	}

	public Boolean sendMail(List<String> emails, String subject, String body) {

		try {
			for (String mail : emails) {
				MimeMessage message = sender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(message);
				helper.setText(body);
				helper.setTo(mail);
				helper.setSubject(subject);
				try {
					sender.send(message);
					System.out.println("Mail has been sent to==>"+mail);
				} catch (Exception e) {
					System.out.println("In valid mail id==>"+mail);
				}
			}

		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public Boolean sendMail(String[] to, String text, String subject) {

		try {
			MimeMessage message = sender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message);
			helper.setText(text);
			helper.setTo(to);
			helper.setSubject(subject);
			sender.send(message);

		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
