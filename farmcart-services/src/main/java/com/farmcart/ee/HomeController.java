package com.farmcart.ee;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RestController;  
@RestController  
public class HomeController extends Thread{  
    @RequestMapping("/hello")  
    public String hello(){  
        return"Hello!";  
    }  
    
    
    @RequestMapping("/")  
    @CrossOrigin("http://localhost:8080")
    public String home(){  
        return"Hello Dadasaheb welcome to farmcart!";  
    }  
}  	