package com.farmcart.ee.company.companycrud;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.farmcart.ee.bean.CustomerBean;
import com.farmcart.ee.interfaces.KeyValueAggregation;

public interface CustomerCRUD extends CrudRepository<CustomerBean, Integer> {
	
	@Query(value="select c.name from  CustomerBean c where c.id =:customerId")
	public String customerNameById(@Param("customerId")Integer customerId);
	
	@Query(value="select c from  CustomerBean c where c.id =:customerId and c.userName =:userName")
	public CustomerBean getById(@Param("customerId")Integer customerId,@Param("userName")String userName );
	
	@Query(value="select c from  CustomerBean c where c.userName =:userName and c.isActive=1")
	public List<CustomerBean> getCustomerList(@Param("userName")String userName);
	
	@Query(value="select count(id) as value, extract(month from created_at) as key   from customer where user_name =:userName group by key",nativeQuery = true)
	public List<KeyValueAggregation> getCustomerCountPerMonth(@Param("userName")String userName);
	

}
