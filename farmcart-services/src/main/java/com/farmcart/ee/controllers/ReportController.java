package com.farmcart.ee.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.farmcart.ee.bean.DefaultResponse;
import com.farmcart.ee.bean.TodaysOrder;
import com.farmcart.ee.company.companycrud.ReportCRUD;
import com.farmcart.ee.report.Report;
import com.farmcart.ee.service.ReportService;

@RestController
public class ReportController {

	@Autowired
	ReportService reportService;

	@Autowired
	ReportCRUD reportCRUD;

	@GetMapping(name="",value="/todaysorderreport/{reportId}")
	public ResponseEntity<DefaultResponse> getReportData(@PathVariable(name="reportId") Integer reportId) {

		DefaultResponse<TodaysOrder> defaultResponse= reportService.getResultForReport(reportId);
		
		return new ResponseEntity<DefaultResponse>(defaultResponse, HttpStatus.OK);
	}

	@GetMapping(name="" ,value="/reportlist")
	public List<Report> getReportList() {
		List<Report> reports = new ArrayList<Report>();
		reportCRUD.findAll().forEach(ele -> {
			reports.add(ele);
		});
		return reports;
	}

}
