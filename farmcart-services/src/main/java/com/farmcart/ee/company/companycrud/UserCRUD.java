package com.farmcart.ee.company.companycrud;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.farmcart.ee.bean.UserBean;

public interface UserCRUD extends CrudRepository<UserBean, Integer>{
	
	@Query("select u from UserBean u where u.userName = :userName")
	public List<UserBean> findByUserName(@Param("userName") String userName);

	@Query("select u from UserBean u where u.userName = :userName and u.password= :password")
	public UserBean findByUserNameAndPassword(@Param("userName")String userName,@Param("password") String password);

}
