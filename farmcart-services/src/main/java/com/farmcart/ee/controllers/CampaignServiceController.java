package com.farmcart.ee.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.farmcart.ee.bean.constant.Constants;
import com.farmcart.ee.company.companycrud.CampaignDetail;
import com.farmcart.ee.company.companycrud.CampaignDetailCRUDService;
import com.farmcart.ee.company.companycrud.CampaignRule;
import com.farmcart.ee.service.CampaignEvalutionService;
import com.farmcart.ee.service.CampaignService;

@RestController
public class CampaignServiceController {

	
	@Autowired
	CampaignDetailCRUDService campaignDetailCRUDService;
	
	@Autowired
	CampaignEvalutionService campaignEvalutionService;
	
	@Autowired
	CampaignService campaignService;

	@RequestMapping(value = "/campaignrule", method = RequestMethod.GET)
	public ResponseEntity getAllOrders(
			@RequestParam(required=false , defaultValue=Constants.DEFAULT_OFFSET) Integer offset,
			@RequestParam(required=false, defaultValue=Constants.DEFAULT_LIMIT) Integer limit,
			@RequestParam(required=false,defaultValue="name") String query,
			@RequestParam(required=false, defaultValue="") String order)
	{
		List<CampaignRule> campaignRules=null;
		ResponseEntity response=null;
		
		try {
			
			campaignRules=campaignService.getCampaignRules(limit, offset, query, order);
			response=new ResponseEntity<>(campaignRules,HttpStatus.OK);
		} catch (Exception e) {
			campaignRules=new ArrayList<>();
			 response=new ResponseEntity<>(campaignRules,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;

	}

	@PostMapping
	public boolean saveCampaign(@RequestBody CampaignDetail campaignDetail) {
		campaignDetailCRUDService.save(campaignDetail);
		System.out.println("Campaign Details has been saved");
		return true;
	}

	@GetMapping(value = "/campaigns")
	public List<CampaignDetail> getCampaigns() {
		List<CampaignDetail> campaignList = new ArrayList<CampaignDetail>();
		
		campaignDetailCRUDService.findAll().forEach(ele -> {
			campaignList.add(ele);
		});
		return campaignList;
	}

	@DeleteMapping(value = "/deletecampaign/{campaignId}")
	public void delete(@PathVariable Long campaignId) {
		System.out.println("Delete Has been called for campaignId==>" + campaignId);
		campaignDetailCRUDService.deleteById(campaignId);

	}

	@GetMapping(value = "/campaign/{campaignId}")
	public CampaignDetail getCampaignById(@PathVariable Long campaignId) {
		CampaignDetail campDetails = null;
		campDetails = campaignDetailCRUDService.findById(campaignId).get();
		return campDetails;

	}

	@GetMapping(value = "/lunchcampaign/{campaignId}")
	public Boolean lunchCampaign(@PathVariable Long campaignId) {
		try {
			CampaignDetail campDetails  = campaignDetailCRUDService.findById(campaignId).get();
			if(campDetails!=null)
			{
				Optional<CampaignRule> camRule=campaignService.getCampaignRule(campDetails.getRuleNumber());
				if(camRule.isPresent())
				{
					campaignEvalutionService.evaluteCampaign(campDetails,camRule);
				}
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
