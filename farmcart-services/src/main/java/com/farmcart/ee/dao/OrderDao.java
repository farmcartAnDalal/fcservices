package com.farmcart.ee.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.jboss.logging.Logger;

import com.farmcart.ee.bean.Order;
import com.farmcart.ee.company.companycrud.OrderCRUDService;

public class OrderDao {

	Logger logger = Logger.getLogger(OrderDao.class);

	OrderCRUDService orderCRUDService;

	public void setController(OrderCRUDService orderCRUDService2) {

		orderCRUDService = orderCRUDService2;
	}


	public void saveOder(Order order) {
		logger.info("order has been done from client====>" + order.getCustomerID());
		orderCRUDService.save(order);
		logger.info("Order has been saved with order id===>" + order.getOrderID());
	}

	public List<Order> getOrders(Integer type) {
		List<Order> orders = new ArrayList<Order>();
		orderCRUDService.findAll().forEach(ele -> {
			if(type==3 && ele.getIsActive()==1) {
				orders.add(ele);
			}else if (ele.getIsActive() == 1 && ele.getOrderType()==type) {
				orders.add(ele);
			}
		});
		return orders;
	}

	public Map<Date, Integer> getProductSoldByDays(int days) {
		List<Order> orders = getOrders(0);
		Calendar cal = Calendar.getInstance();
		int count = 0;
		Map<Date, Integer> productSoldByDays = new LinkedHashMap<>();
		for (int i = 0; i < days; i++) {

			count = 0;
			cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, -i);
			Date date = cal.getTime();
			String stringDate = cal.get(Calendar.DATE) + "/" + cal.get(Calendar.MONTH) + "/" + cal.get(Calendar.YEAR);
			Iterator<Order> it = orders.iterator();
			Order order = new Order();

			while (it.hasNext()) {
				order = it.next();
				Calendar orderCal = Calendar.getInstance();
				orderCal.setTime(order.getCreatedAt());
				String orderStringDate = orderCal.get(Calendar.DATE) + "/" + orderCal.get(Calendar.MONTH) + "/"
						+ orderCal.get(Calendar.YEAR);
				if (stringDate.equals(orderStringDate)) {
					count++;
				}
			}

			productSoldByDays.put(date, count);

		}

		return productSoldByDays;
	}

}
