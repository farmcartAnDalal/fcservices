package com.farmcart.ee.bean;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.social.facebook.api.FacebookObject;
import org.springframework.social.facebook.api.MessageTag;
import org.springframework.social.facebook.api.Reference;
import org.springframework.social.facebook.api.StoryAttachment;


/**
 * Model class representing a comment.
 * @author Craig Walls
 */
public class Comment extends FacebookObject {
	
	private static final List<MessageTag> EMPTY_TAG_LIST = Collections.emptyList();
	
	private String id;
	
	private StoryAttachment attachment;
	
	private boolean canComment;
	
	private boolean canRemove;
	
	private Integer commentCount;
	
	private Date createdTime;
	
	private Reference from;
	
	private Integer likeCount;
	
	private String message;
	
	private List<MessageTag> messageTags;

	private Comment parent;

	private boolean userLikes;
	
	/**
	 * @return the comment's Graph API object ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the text of the comment
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @return the time the comment was created.
	 */
	public Date getCreatedTime() {
		return createdTime;
	}

	/**
	 * @return a reference to the user who posted the comment.
	 */
	public Reference getFrom() {
		return from;
	}

	/**
	 * @return the number of users who like this comment.
	 */
	public Integer getLikeCount() {
		return likeCount;
	}
	
	/**
	 * @return the number of comments made on this comment or null if that information is unknown
	 */
	public Integer getCommentCount() {
		return commentCount;
	}
	
	/**
	 * @return the parent comment if this comment is a comment to another comment
	 */
	public Comment getParent() {
		return parent;
	}
	
	/**
	 * @return true if the authenticated user is able to comment on this comment
	 */
	public boolean canComment() {
		return canComment;
	}
	
	/**
	 * @return true if the authenticated user is able to remove this comment
	 */
	public boolean canRemove() {
		return canRemove;
	}
	
	/**
	 * @return true if the authenticated user likes this comment
	 */
	public boolean userLikes() {
		return userLikes;
	}
	
	/**
	 * @return an attachment (link, photo, etc) associated with the comment or null if no attachment
	 */
	public StoryAttachment getAttachment() {
		return attachment;
	}

	/**
	 * @return a list of tags in the comment's message
	 */
	public List<MessageTag> getMessageTags() {
		return messageTags != null ? messageTags : EMPTY_TAG_LIST;
	}

}
