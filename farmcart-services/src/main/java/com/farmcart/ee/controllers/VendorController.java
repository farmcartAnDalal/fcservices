package com.farmcart.ee.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.farmcart.ee.bean.VendorBean;
import com.farmcart.ee.bean.constant.Constants;
import com.farmcart.ee.bean.constant.Constants.ACTION;
import com.farmcart.ee.bean.constant.Constants.ENTITY;
import com.farmcart.ee.bean.constant.EEUtil;
import com.farmcart.ee.company.companycrud.VendorCRUD;
import com.farmcart.ee.service.AuditLogService;

@RestController
public class VendorController {

	@Autowired
	VendorCRUD vendorCRUD;
	
	@Autowired
	private AuditLogService auditLogService;
	
	@PostMapping(value = "/vendor")
	public VendorBean saveVendor(@RequestBody VendorBean vendorBean,@RequestHeader("Authorization") String token) {
		String userName = Constants.getUserName(token);	
		vendorBean.setUserName(userName);
		vendorBean.setCreatedAt(new Date());
		vendorBean.setUpdatedAt(new Date());
		vendorCRUD.save(vendorBean);
		
		auditLogService.saveAuditLog(EEUtil.getAuditLogBean(userName, ACTION.CREATE.name(),vendorBean.getName(), ENTITY.VENDOR.name()));
		return vendorBean;
	}

	@GetMapping(value = "/vendor/{id}")
	public VendorBean getVendorBean(@PathVariable Integer id,@RequestHeader("Authorization") String token) {
		String userName = Constants.getUserName(token);	
		return vendorCRUD.getVendorById(userName, id);
	}

	@GetMapping(value = "/vendors")
	public List<VendorBean> getVendors(@RequestHeader("Authorization") String token) {
		String userName = Constants.getUserName(token);	
		return vendorCRUD.getVendorList(userName);
	}

	@DeleteMapping(value = "/deletevendor/{id}")
	public Boolean deleteVendor(@PathVariable Integer id,@RequestHeader("Authorization") String token) {
		String userName = Constants.getUserName(token);	
		VendorBean vendor = vendorCRUD.getVendorById(userName,id);
		vendor.setIsActive(0);
		vendorCRUD.save(vendor);
		auditLogService.saveAuditLog(EEUtil.getAuditLogBean(userName, ACTION.DELETE.name(),vendor.getName(), ENTITY.VENDOR.name()));
		return true;
	}
}
