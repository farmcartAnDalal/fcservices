package com.farmcart.ee.enums;

public enum ReportEnum {

	TodaysOrder(1, "Todays Order"), 
	WeeklyGrossProfit(2, "WeeklyGross profit"),
	WeeklyProductSell(3, "Weekly Product Sell"), 
	NDaysGrossProfit(4, "N Days Gross Profit"),
	NDaysProductSell(5, "N Days product Sell");

	private Integer id;
	private String name;

	ReportEnum(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
