package com.farmcart.ee.company.companycrud;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

public interface CampaignRuleCRUD extends CrudRepository<CampaignRule, Long>  {

	Iterable<CampaignRule> findAll(Pageable pageable);

	
}
