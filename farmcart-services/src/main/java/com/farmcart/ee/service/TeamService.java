package com.farmcart.ee.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.farmcart.ee.bean.TeamMember;
import com.farmcart.ee.company.companycrud.TeamMemberCRUD;

@Service
public class TeamService {
	@Autowired
	TeamMemberCRUD memberCRUD;

	public List<TeamMember> getListOfTeamMembers() {
		List<TeamMember> list = new ArrayList<>();
		memberCRUD.findAll().forEach(ele -> {
			list.add(ele);
		});
		return list;
	}
	
	public void save(TeamMember member)
	{
		memberCRUD.save(member);
	}
	
	public  void delete(Long id)
	{
		memberCRUD.deleteById(id);
	}

}
