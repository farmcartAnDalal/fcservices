package com.farmcart.ee.controllers;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.farmcart.ee.bean.Order;
import com.farmcart.ee.bean.OrderItem;
import com.farmcart.ee.bean.ProductBean;
import com.farmcart.ee.bean.constant.Constants;
import com.farmcart.ee.bean.constant.Constants.ACTION;
import com.farmcart.ee.bean.constant.Constants.ENTITY;
import com.farmcart.ee.bean.constant.EEUtil;
import com.farmcart.ee.company.companycrud.CustomerCRUD;
import com.farmcart.ee.company.companycrud.OrderCRUDService;
import com.farmcart.ee.company.companycrud.ProductCRUDService;
import com.farmcart.ee.dao.OrderDao;
import com.farmcart.ee.interfaces.KeyValueAggregation;
import com.farmcart.ee.service.AuditLogService;

@RestController
public class OrderService {

	Logger logger = Logger.getLogger(OrderDao.class);

	@Autowired
	OrderCRUDService orderCRUDService;

	@Autowired
	ProductCRUDService productCRUDService;

	@Autowired
	CustomerCRUD customerCRUDRepo;
	
	@Autowired
	private AuditLogService auditLogService;
	

	@RequestMapping(value = "/order/list/{type}", method = RequestMethod.GET)
	public List<Order> getAllOrders(@PathVariable(name = "type") Integer orderType,
			@RequestHeader("Authorization") String token) {
		String userName = Constants.getUserName(token);
		if (orderType == 3) {
			return orderCRUDService.getOrders(userName);
		}
		return orderCRUDService.getOrderByType(orderType, userName);

	}

	@RequestMapping(value = "/Order/{id}", method = RequestMethod.GET)
	public Order getOrderbyId(@PathVariable("id") Long id) {
		Order order = orderCRUDService.findById(id).get();
		return order;

	}

	@RequestMapping(value = "/Order", method = RequestMethod.POST)
	public ResponseEntity<Order> saveOrder(@RequestBody Order order, @RequestHeader("Authorization") String token) {
		String userName = Constants.getUserName(token);
		order.setUserName(userName);
		logger.info("order has been done from client====>" + order.getCustomerID());
		order.setCreatedAt(new Date());
		if (order.getOrderItems() != null && !order.getOrderItems().isEmpty()) {
			List<OrderItem> items = order.getOrderItems().stream().map(ele -> {
				ele.setOrderDate(order.getOrderDate());
				return ele;
			}).collect(Collectors.toList());
			order.setOrderItems(items);
		}

		orderCRUDService.save(order);
		logger.info("Order has been saved with order id===>" + order.getOrderID());
		order.getOrderItems().stream().forEach(ele -> {
			ProductBean pb = productCRUDService.findById(ele.getItemID().intValue()).get();
			Long newQuantity = 0l;
			if (order.getOrderType() == 0) {
				newQuantity = pb.getQuantity() - ele.getQuantity();
			} else {
				newQuantity = pb.getQuantity() + ele.getQuantity();
			}

			pb.setQuantity(newQuantity.intValue());
			productCRUDService.save(pb);

		});
		
		if(order.getOrderType()==0) {
			
			auditLogService.saveAuditLog(EEUtil.getAuditLogBean(userName, ACTION.CREATE.name(),order.getCustomerName(), ENTITY.ORDER.name()));
		}else {
			auditLogService.saveAuditLog(EEUtil.getAuditLogBean(userName, ACTION.CREATE.name(),order.getCustomerName(), ENTITY.PURCHASE.name()));			
		}
		return new ResponseEntity<>(order, HttpStatus.OK);
	}

	@RequestMapping(value = "/Order/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> updateOrder(@RequestBody Order order, @PathVariable Long id) {
		order.setOrderID(id);
		orderCRUDService.save(order);
		
		if(order.getOrderType()==0) {
			auditLogService.saveAuditLog(EEUtil.getAuditLogBean(order.getUserName(), ACTION.EDIT.name(),order.getCustomerName(), ENTITY.ORDER.name()));
		}else {
			auditLogService.saveAuditLog(EEUtil.getAuditLogBean(order.getUserName(), ACTION.EDIT.name(),order.getCustomerName(), ENTITY.PURCHASE.name()));			
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/productsoldbydays/{days}", method = RequestMethod.GET)
	public Map<Date, Integer> getProductSoldByDays(@PathVariable int days) {

		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		OrderDao orderDao = (OrderDao) ctx.getBean("edao");
		orderDao.setController(orderCRUDService);

		return orderDao.getProductSoldByDays(7);
	}

	@DeleteMapping(value = "/Order/{id}")
	public ResponseEntity<Void> deletOrder(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) {
		String userName = Constants.getUserName(token);
		Order order = orderCRUDService.findById(id).get();
		order.setIsActive(0);
		orderCRUDService.save(order);
		if(order.getOrderType()==0) {
			auditLogService.saveAuditLog(EEUtil.getAuditLogBean(order.getUserName(), ACTION.DELETE.name(),order.getCustomerName(), ENTITY.ORDER.name()));
		}else {
			auditLogService.saveAuditLog(EEUtil.getAuditLogBean(order.getUserName(), ACTION.DELETE.name(),order.getCustomerName(), ENTITY.ORDER.name()));
		}
		logger.info("Delete Request has been called for Order==>" + id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping(value = "/orderamountperday/{day}")
	public List<KeyValueAggregation> getOrderAmountPerday(@RequestHeader("Authorization") String token,
			@PathVariable("day") Integer day) {
		String userName = Constants.getUserName(token);
		Calendar cal = Calendar.getInstance();
		if (day == 1) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
			Date tomorrow = cal.getTime();
			return orderCRUDService.orderTodays(userName, tomorrow);
		} else if (day == 2) {
			cal.add(Calendar.DAY_OF_MONTH, -8);
			Date sevenDayBack = cal.getTime();
			return orderCRUDService.orderPerWeek(userName, sevenDayBack);
		} else if (day == 3) {
			return orderCRUDService.orderPerMonth(userName);
		} else if (day == 4) {
			return orderCRUDService.orderPerYear(userName);
		}
		List<KeyValueAggregation> keyValues = orderCRUDService.getPerdayOrderAmount(userName);
		return keyValues;
	}

	@GetMapping(value = "/sellPurchaseCount/{day}")
	public List<KeyValueAggregation> getSellPurchaseCount(@RequestHeader("Authorization") String token,
			@PathVariable("day") Integer day) {
		String userName = Constants.getUserName(token);

		Calendar cal = Calendar.getInstance();
		if (day == 1) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
			Date tomorrow = cal.getTime();
			return orderCRUDService.getSellPurchaseAmount(userName, tomorrow);
		} else if (day == 2) {
			cal.add(Calendar.DAY_OF_MONTH, -8);
			Date sevenDayBack = cal.getTime();
			return orderCRUDService.getSellPurchaseAmount(userName, sevenDayBack);
		} else if (day == 3) {
			cal.add(Calendar.DAY_OF_MONTH, -30);
			Date oneMonthBack = cal.getTime();
			return orderCRUDService.getSellPurchaseAmount(userName, oneMonthBack);
		} else if (day == 4) {
			cal.add(Calendar.DAY_OF_MONTH, -365);
			Date oneYearback = cal.getTime();
			return orderCRUDService.getSellPurchaseAmount(userName, oneYearback);
		}
		List<KeyValueAggregation> keyValues = orderCRUDService.getSellPurchaseAmount(userName, new Date());
		return keyValues;
	}

	@GetMapping(value = "/orderavgamount")
	public List<KeyValueAggregation> getOrderAmountPerMonth(@RequestHeader("Authorization") String token) {
		String userName = Constants.getUserName(token);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -365);
		Date from = cal.getTime();
		return orderCRUDService.avgOrderPrice(userName, from);
	}
	
	@GetMapping(value = "/ordercountpermonths")
	public List<KeyValueAggregation> getOrderCountPerMonths(@RequestHeader("Authorization") String token) {
		String userName = Constants.getUserName(token);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -365);
		Date from = cal.getTime();
		return orderCRUDService.orderCountPerMonths(userName, from);
	}
	

}
