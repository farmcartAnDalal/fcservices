CREATE TABLE account(
 user_id serial PRIMARY KEY,
 username VARCHAR (50) UNIQUE NOT NULL,
 password VARCHAR (50) NOT NULL,
 email VARCHAR (355) UNIQUE NOT NULL,
 created_on TIMESTAMP NOT NULL,
 last_login TIMESTAMP
);


insert into users values (1,'FD','fd@123','dadakambale18@gmail.com',to_timestamp('16-05-2011 15:36:38', 'dd-mm-yyyy hh24:mi:ss'),null);

create table product_bean (
       id  serial not null,
        created_at timestamp,
        description varchar(255),
        name varchar(255),
        primary key (id)
    );
    
    create table user_bean (
       id  serial not null,
        first_name varchar(255),
        last_name varchar(255),
        password varchar(255),
        user_name varchar(255),
        primary key (id)
    )

    insert into campaign_rule values(1,'Total Weekly Revenue Greater Than');
	insert into campaign_rule values(2,'Total Montly Revenue Greater Than');
	insert into campaign_rule values(3,'All');
	insert into campaign_rule values(4,'Average Weekly Revenue');
	insert into campaign_rule values(5,'Total Weekly Revenue Less Than');
	insert into campaign_rule values(6,'Total Weekly Revenue Equal');
	insert into campaign_rule values(7,'Total Montly Revenue Greater Less Than');
	insert into campaign_rule values(8,'Total Montly Revenue Equal ');
	insert into campaign_rule values(9,'Send to Specific Customers');
	insert into campaign_rule values(10,'Specific Product Offer Product Name');

insert into report values(1,'Todays Order');
insert into report values(2,'WeeklyGross profit');
insert into report values(3,'Weekly Product Sell');
insert into report values(4,'N Days Gross Profit');
insert into report values(5,'N Days product Sell');


select sum(g_total), extract(year from order_date) as year from order_bean group by year;

select sum(g_total), extract(month from order_date) as month   from order_bean group by month;

select sum(g_total), extract(day from order_date) as day   from order_bean group by day;

select * from order_bean;

SELECT sum(g_total), extract(day from order_date) as day  FROM order_bean WHERE order_date > now()::date - 7 group by day;

SELECT sum(g_total), extract(day from order_date) as day  FROM order_bean WHERE order_date > now()::date - 1 group by day;




