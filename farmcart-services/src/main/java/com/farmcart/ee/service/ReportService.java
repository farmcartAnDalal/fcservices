package com.farmcart.ee.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.farmcart.ee.bean.DefaultResponse;
import com.farmcart.ee.bean.TodaysOrder;
import com.farmcart.ee.bean.constant.Constants;
import com.farmcart.ee.enums.ReportEnum;

@Component
public class ReportService {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public DefaultResponse getResultForReport(Integer reportId) {
		DefaultResponse defaultResponse = null;
		if (ReportEnum.TodaysOrder.getId() == reportId) {
			defaultResponse = getTodaysOrder();
		}
		return defaultResponse;
	}

	private DefaultResponse getTodaysOrder() {
		try {
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			c.add(Calendar.DATE, -8); 
			String output = Constants.sdf.format(c.getTime());
			String query="select o.customerid,c.email_id ,(o.g_total),o.order_no from customer c,order_bean o where c.id=o.customerid and o.created_at>'"+output+"'"; 
					
			return jdbcTemplate.query(query, new ResultSetExtractor<DefaultResponse<TodaysOrder>>() {
				@Override
				public DefaultResponse<TodaysOrder> extractData(ResultSet rs) throws SQLException, DataAccessException {
					DefaultResponse<TodaysOrder> result=new DefaultResponse<>();
					List<TodaysOrder> list = new ArrayList<TodaysOrder>();
					while (rs.next()) {
						TodaysOrder e = new TodaysOrder();
						e.setCustmorId(rs.getLong(1));
						e.setEmailId(rs.getString(2));
						e.setOrderNumber(rs.getString(3));
						e.setTotal(rs.getDouble(4));
						list.add(e);
					}
					System.out.println("Order size=====>"+list.size());
					result.setData(list);
					return result;
				}
			});
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error occured while doing query==>TodaysOrder");
			return new DefaultResponse<TodaysOrder>();
			
		}
		
	}

}
