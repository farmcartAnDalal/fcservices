package com.farmcart.ee.company.companycrud;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.farmcart.ee.bean.Order;
import com.farmcart.ee.interfaces.KeyValueAggregation;

public interface OrderCRUDService extends CrudRepository<Order, Long> {
	
	@Query("select orderDate as key,sum(gTotal) as value from Order o where o.userName= :userName group by o.orderDate")
	public List<KeyValueAggregation> getPerdayOrderAmount(@Param("userName") String userName );
	
	@Query("select orderType as key,sum(gTotal) as value from Order o where o.userName= :userName and o.orderDate> :from group by o.orderType")
	public List<KeyValueAggregation> getSellPurchaseAmount( @Param("userName") String userName,@Param("from")Date from );
	
	@Query("select o from Order o where o.userName =:userName and o.orderType =:orderType ")
	public List<Order> getOrderByType(@Param("orderType")Integer orderType,@Param("userName") String userName );
	
	@Query("select o from Order o where o.userName =:userName ")
	public List<Order> getOrders(@Param("userName") String userName );
	
	@Query(value = "SELECT sum(g_total) as value, extract(day from order_date) as key  FROM order_bean WHERE user_name =:userName and order_date > :sevenDayBack group by key", nativeQuery = true)
	public List<KeyValueAggregation> orderPerWeek(@Param("userName") String userName,@Param("sevenDayBack")  Date sevenDayBack);
	
	@Query(value = "SELECT sum(g_total) as value, extract(day from order_date) as key  FROM order_bean WHERE order_date > :tomorrow and user_name =:userName group by key", nativeQuery = true)
	public List<KeyValueAggregation> orderTodays( @Param("userName") String userName,@Param("tomorrow") Date tomorrow );
	
	@Query(value = "select sum(g_total) as value, extract(month from order_date) as key from order_bean   where user_name =:userName group by key", nativeQuery = true)
	public List<KeyValueAggregation> orderPerMonth(@Param("userName") String userName );
	
	@Query(value = "select sum(g_total) as value, extract(year from order_date) as key from order_bean  where user_name =:userName group by key", nativeQuery = true)
	public List<KeyValueAggregation> orderPerYear(@Param("userName") String userName );
	
	@Query(value = "select avg(g_total) as value, extract(month from order_date) as key from order_bean where order_date > :from and user_name =:userName group by key", nativeQuery = true)
	public List<KeyValueAggregation> avgOrderPrice(@Param("userName") String userName ,@Param("from")Date from );
	
	

	
	@Query(value = "select count(g_total) as value, extract(month from order_date) as key from order_bean   where order_date > :from and  user_name =:userName group by key", nativeQuery = true)
	public List<KeyValueAggregation> orderCountPerMonths(@Param("userName") String userName,@Param("from")Date from  );
	

}
