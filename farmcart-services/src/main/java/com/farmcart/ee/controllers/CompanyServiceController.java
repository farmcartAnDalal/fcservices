package com.farmcart.ee.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.farmcart.ee.bean.AddressBean;
import com.farmcart.ee.bean.CompanyBean;
import com.farmcart.ee.company.companycrud.CompanyCRUDServices;

@RestController
public class CompanyServiceController {

	@Autowired
	private CompanyCRUDServices cService;

	@RequestMapping(value = "/company", method = RequestMethod.GET)
	public List<CompanyBean> getAllCompanies() {
		AddressBean  ab=new AddressBean("Mkc Residency", "flat No-5", "Solapur", "Maharastra", "India", 413412);
		CompanyBean cb=new CompanyBean(1, "DOPPK9575E", "Farmcart Tech LLP", ab);
		cService.save(cb);
		final List<CompanyBean> allCompanies = new ArrayList<CompanyBean>();
		cService.findAll().forEach(ele -> allCompanies.add(ele));
		return allCompanies;
	}
	
	@RequestMapping(value = "/company/{id}", method = RequestMethod.GET)
	public Optional<CompanyBean> getCompany(@PathVariable Integer id) {
		return cService.findById(id);
	}

	@RequestMapping(value = "/company", method = RequestMethod.POST)
	public void registerCompany(@RequestBody CompanyBean company) {
		cService.save(company);
		return;
	}

	@RequestMapping(value = "/company/{id}", method = RequestMethod.DELETE)
	public void deleteCompany(@PathVariable Integer id) {
		cService.deleteById(id);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/company/{id}")
	public void updateCompany(@RequestBody CompanyBean company, @PathVariable Integer id) {
		company.setCompanyId(id);
		cService.save(company);
	}

}
