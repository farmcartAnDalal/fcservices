package com.farmcart.ee.company.companycrud;

import org.springframework.data.repository.CrudRepository;

import com.farmcart.ee.bean.TeamMember;

public interface TeamMemberCRUD extends CrudRepository<TeamMember, Long>{

}
