package com.farmcart.ee.user.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

public class FileStorageService {

	private final Path fileStorageLocation = Paths.get("dada", "fd");

	public String storeFile(MultipartFile file) {
		// Normalize file name
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());

		try {
			// Check if the file's name contains invalid characters
			if (fileName.contains("..")) {
				throw new Exception("Sorry! Filename contains invalid path sequence " + fileName);
			}

			// Copy file to the target location (Replacing existing file with the same name)

			Path path = Paths.get(
					"/Users/dadasahebkambale/Documents/backend/fcservices/farmcart-services/src/main/java/com/farmcart/ee/user/controller/FileStorageService.java");

			File targetFile = new File(
					"/Users/dadasahebkambale/Documents/workspace-spring-tool-suite-4-4.0.0.RELEASE/farmcart-services/src/main/java/com/farmcart/ee/user/controller/"
							+ fileName);
			OutputStream outStream = new FileOutputStream(targetFile);

			byte[] buffer = new byte[8 * 1024];
			int bytesRead;
			while ((bytesRead = file.getInputStream().read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}

			IOUtils.closeQuietly(outStream);
			Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);

			return fileName;
		} catch (IOException ex) {
			try {
				throw new Exception("Could not store file " + fileName + ". Please try again!", ex);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	public Resource loadFileAsResource(String fileName) {
		try {
			//Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
			Path filePath = Paths.get(
					"/Users/dadasahebkambale/Documents/backend/fcservices/farmcart-services/src/main/java/com/farmcart/ee/user/controller/FileStorageService.java");

			Resource resource = new UrlResource(filePath.toUri());
			if (resource.exists()) {
				return resource;
			} else {
				throw new Exception("File not found " + fileName);
			}
		} catch (Exception ex) {
			return null;
		}
	}
}
