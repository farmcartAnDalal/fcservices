package com.farmcart.ee.bean.constant;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import com.farmcart.ee.bean.UserBean;

public class Constants {

	public static Map<String, String> map = new HashMap<String, String>();
	public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	public static final String DEFAULT_LIMIT = "50";
	public static final String DEFAULT_OFFSET = "0";

	public static Map<String, UserBean> userWithToken = new HashMap<>();

	enum COLNAME {

		NAME("name");

		private String colName;

		COLNAME(String colName) {
			this.colName = colName;
		}

		public String getColName() {
			return colName;
		}
	}

	public static String getColumnName(String query) {

		if (COLNAME.NAME.getColName().equals(query))
			return COLNAME.NAME.getColName();

		return "";
	}

	public static String getUserName(String authcode) {
		String token = authcode.split(" ")[1];
		UserBean user = userWithToken.get(token);
		if (null != user) {
			return user.getUserName();
		}
		return null;
	}

	public enum ACTION {

		LOGIN, LOGOUT, CREATE, EDIT, DELETE
	}

	public enum ENTITY {

		PRODUCT, ORDER, CUSTOMER, PURCHASE, VENDOR, HOME
	}

}
