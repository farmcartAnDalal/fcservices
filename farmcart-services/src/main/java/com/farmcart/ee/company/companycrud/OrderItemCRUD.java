package com.farmcart.ee.company.companycrud;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.farmcart.ee.bean.OrderItem;
import com.farmcart.ee.interfaces.KeyValueAggregation;

public interface OrderItemCRUD  extends CrudRepository<OrderItem, Long> {
	
	@Query("select o.orderDate as key,sum(o.quantity) as value from OrderItem o where o.userName = :userName  and o.orderDate > :from  group by o.orderDate")
	public List<KeyValueAggregation> getProductSellperDay( @Param("userName")String userName,@Param("from") Date from);

}
