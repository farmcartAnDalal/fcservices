package com.farmcart.ee.bean;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;

public class FacebookPost {

	@JsonAlias(value= {"data"})
	List<Post> posts;
	@JsonAlias(value= {"paging"})
	Paging paging;

	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	public Paging getPaging() {
		return paging;
	}

	public void setPaging(Paging paging) {
		this.paging = paging;
	}

}
