package com.farmcart.ee.controllers;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.social.facebook.api.Comment;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.farmcart.ee.bean.constant.Constants;
import com.farmcart.ee.service.FacebookScanningService;

@RestController
public class FacebookController {
	
	@Autowired
	FacebookScanningService facebookScanningService;

	@GetMapping(value="/connectFacebook" , produces = "application/json")
	public ResponseEntity<String> getAutorize() {
		HttpHeaders headers = new HttpHeaders();
		String authorizeUrl = "https://www.facebook.com/v2.5/dialog/oauth?client_id=2069296173370724&response_type=code&redirect_uri=http://localhost:8080/connectFacebookCallBack&scope=user_posts+manage_pages+publish_pages&state=6646c20d-f61e-4984-ae64-abcc92f0d8a3&display=popup'";
		try {
			headers.setLocation(new URI(authorizeUrl));
		} catch (URISyntaxException e) {

			e.printStackTrace();
		}
		return new ResponseEntity<String>("Hello dadasaheb", headers, HttpStatus.MOVED_PERMANENTLY);
	}

	@GetMapping(value = "/connectFacebookCallBack", produces = "application/json", params = { "code", "state" })
	public ResponseEntity<String> getAccessCode(@RequestParam(value = "code") String code,
			@RequestParam(value = "state") String state) {

		HttpHeaders headers = new HttpHeaders();
		String access_token = null;
		try {
			headers.setLocation(new URI("http://localhost:4200/home/reputations"));
			access_token =facebookScanningService.getAccessTokenFromCode(code);
			Constants.map.put("access_token", access_token);
			System.out.println("Access Token==>" + access_token);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}

		return new ResponseEntity<String>(access_token, headers, HttpStatus.MOVED_PERMANENTLY);
	}

	@GetMapping(value = "/facebookposts")
	public ResponseEntity<PagedList<org.springframework.social.facebook.api.Post>> getFaceBookPost()
	{
		PagedList<org.springframework.social.facebook.api.Post> posts=facebookScanningService.getFaceBookPosts();
		return new ResponseEntity<PagedList<org.springframework.social.facebook.api.Post>>(posts,HttpStatus.OK);
	}
	
	@GetMapping("/getCommentsForPostId/{postId}")
	public PagedList<Comment> getCommentsForPost(@PathVariable String postId)
	{
		return facebookScanningService.getCommentsForPost(postId);
		
	}
	
	@GetMapping("/likes/{postId}")
	public PagedList<Reference> getLikes(@PathVariable String postId)
	{
		return facebookScanningService.getLikesForPost(postId);
		
	}
	
	@PostMapping("/createNewPost")
	public Boolean createNewPost()
	{
		return true;
	}

	
}
