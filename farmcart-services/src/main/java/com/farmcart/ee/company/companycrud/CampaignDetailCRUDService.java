package com.farmcart.ee.company.companycrud;

import org.springframework.data.repository.CrudRepository;

public interface CampaignDetailCRUDService extends CrudRepository<CampaignDetail, Long>  {

}
