package com.farmcart.ee.bean;

public class TodaysOrder {

	Long custmorId;
	String emailId;
	Double total;
	String orderNumber;

	public Long getCustmorId() {
		return custmorId;
	}

	public void setCustmorId(Long custmorId) {
		this.custmorId = custmorId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

}
