package com.farmcart.ee.bean;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "order_bean")
public class Order extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@PrimaryKeyJoinColumn
	Long orderID;
	String orderNo;
	Long customerID;
	String pmethod;
	Double gTotal;
	Integer orderType;
	String customerName;
	Double amountPaid;
	Double totalTax;
	Double totalDiscount;
	Long balanceAmount;
	Long extraCharge;
	String deletedOrderItemIDs;
	Integer isActive;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss")
	Date createdAt;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	Date orderDate;
	
	@Embedded
	@OneToMany(targetEntity = OrderItem.class, cascade = CascadeType.ALL)
	List<OrderItem> orderItems;
	
	public Long getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(Long balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public Long getExtraCharge() {
		return extraCharge;
	}

	public void setExtraCharge(Long extraCharge) {
		this.extraCharge = extraCharge;
	}

	
	public Double getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(Double amountPaid) {
		this.amountPaid = amountPaid;
	}

	public Double getTotalTax() {
		return totalTax;
	}

	public void setTotalTax(Double totalTax) {
		this.totalTax = totalTax;
	}

	public Double getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}


	
	public String getCustomerName() {
		return customerName;
	}
	
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Integer getIsActive() {
		return isActive;
	}

	public Integer getOrderType() {
		return orderType;
	}

	public void setOrderType(Integer orderType) {
		this.orderType = orderType;
	}

	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}

	public Order() {

	}

	public Long getOrderID() {
		return orderID;
	}

	public void setOrderID(Long orderID) {
		this.orderID = orderID;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public Long getCustomerID() {
		return customerID;
	}

	public void setCustomerID(Long customerID) {
		this.customerID = customerID;
	}

	public String getPmethod() {
		return pmethod;
	}

	public void setPmethod(String pmethod) {
		this.pmethod = pmethod;
	}

	public Double getgTotal() {
		return gTotal;
	}

	public void setgTotal(Double gTotal) {
		this.gTotal = gTotal;
	}

	public String getDeletedOrderItemIDs() {
		return deletedOrderItemIDs;
	}

	public void setDeletedOrderItemIDs(String deletedOrderItemIDs) {
		this.deletedOrderItemIDs = deletedOrderItemIDs;
	}

	public List<OrderItem> getOrderItems() {
		return orderItems;
	}

	public void setOrderItems(List<OrderItem> orderItems) {
		this.orderItems = orderItems;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

}
