package com.farmcart.ee.company.companycrud;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.farmcart.ee.bean.VendorBean;

public interface VendorCRUD extends CrudRepository<VendorBean, Integer> {
	
	@Query("select v from VendorBean v where v.userName = :userName")
	public List<VendorBean> getVendorList(@Param("userName") String userName);
	
	@Query("select v from VendorBean v where v.userName = :userName and v.id =:id")
	public VendorBean getVendorById(@Param("userName") String userName ,@Param("id") Integer id);

}
