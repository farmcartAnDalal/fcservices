package com.farmcart.ee.controllers;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.farmcart.ee.bean.UserBean;
import com.farmcart.ee.bean.constant.Constants;
import com.farmcart.ee.bean.constant.Constants.ACTION;
import com.farmcart.ee.bean.constant.Constants.ENTITY;
import com.farmcart.ee.bean.constant.EEUtil;
import com.farmcart.ee.company.companycrud.UserCRUD;
import com.farmcart.ee.service.AuditLogService;

@RestController
public class LoginServiceController {

	@Autowired
	private UserCRUD uService;
	
	@Autowired
	private AuditLogService auditLogService;
	
	

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public boolean login(@PathParam("userName") String userName, @PathParam("password") String password) {

		System.out.println("User:" + userName + "Password:" + password);
		UserBean user = uService.findByUserNameAndPassword(userName, password);
		if (null != user) {
			Constants.userWithToken.put(user.getFirstName() + user.getLastName(), user);
			return true;
		}
		auditLogService.saveAuditLog(EEUtil.getAuditLogBean(userName, ACTION.LOGIN.name(),userName, ENTITY.HOME.name()));
		return false;
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public UserBean login(@RequestBody UserBean user) {

		System.out.println("User:" + user.getUserName() + "Password:" + user.getPassword());
		UserBean loggedInUser = null;
		loggedInUser = uService.findByUserNameAndPassword(user.getUserName(), user.getPassword());
		if (null != loggedInUser) {
			String token = loggedInUser.getFirstName() + loggedInUser.getLastName();
			Constants.userWithToken.put(token, loggedInUser);
			loggedInUser.setToken(token);
			auditLogService.saveAuditLog(EEUtil.getAuditLogBean(user.getUserName(), ACTION.LOGIN.name(),user.getUserName(), ENTITY.HOME.name()));
			return loggedInUser;
		}
		
		return loggedInUser;
	}

}
