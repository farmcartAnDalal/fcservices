package com.farmcart.ee.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.farmcart.ee.bean.FeedbackBean;
import com.farmcart.ee.bean.UserBean;
import com.farmcart.ee.company.companycrud.FeedbackCRUDService;
import com.farmcart.ee.company.companycrud.UserCRUD;

@RestController
public class UserServiceController {

    @Autowired
    private UserCRUD uService;
    
    @Autowired
    private FeedbackCRUDService fService;

    @RequestMapping(method = RequestMethod.POST, value = "/user")
    public void saveUser(@RequestBody UserBean user) {
        System.out.println(user.getDateOfBirth());
        System.out.println("Got hit from service");
        uService.save(user);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/user")
    public List<UserBean> getAllUsers() {
        List<UserBean> allUsers = new ArrayList<UserBean>();
        uService.findAll().forEach(ele -> allUsers.add(ele));
        return allUsers;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/user/{id}")
    public UserBean getUserById(@PathVariable Integer id) {
        return uService.findById(id).get();
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/user/{id}")
    public void updateUser(@RequestBody UserBean user, @PathVariable Integer id) {
        user.setId(id);
        uService.save(user);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/user/{id}")
    public void deleteUser(@PathVariable Integer id) {
        uService.deleteById(id);
    }
    
    @PostMapping(value = "/feedbackForm")
    public void saveFeedback(@RequestBody FeedbackBean feedback)
    {
        fService.save(feedback);
    }
    
    @GetMapping(value="/feedback/list")
    public List<FeedbackBean> getFeeBacks()
    {
    	List<FeedbackBean> feedBackList=new ArrayList<>();
    	fService.findAll().forEach(ele->{feedBackList.add(ele);});
    	return feedBackList;
    }
    
}
